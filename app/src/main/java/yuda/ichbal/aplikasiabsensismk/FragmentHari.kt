package yuda.ichbal.aplikasiabsensismk

import android.app.AlertDialog
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_hari.*
import kotlinx.android.synthetic.main.frag_data_hari.view.*
import kotlinx.android.synthetic.main.frag_data_hari.view.lsHari

class FragmentHari: Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener{

    override fun onClick(v: View?) {
        when(v?.id){

            R.id.btnInsertHari ->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukkan sudah benar?")
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
        }
    }

    lateinit var thisParent : MainActivity
    lateinit var lsAdapter : ListAdapter
    lateinit var spAdapter : SimpleCursorAdapter
    lateinit var dialog : AlertDialog.Builder
    lateinit var v : View
    var namaHari : String = ""
    var namaKelas : String = ""
    lateinit var db : SQLiteDatabase
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_data_hari,container,false)
        db = thisParent.getDbObject()
        dialog = AlertDialog.Builder(thisParent)
        v.btnInsertHari.setOnClickListener(this)
        v.spNamaKelas.onItemSelectedListener = this
        return v
    }

    override fun onStart() {
        super.onStart()
        showDataHari("")
        showDataKelas()
    }

    fun showDataHari(namaHari : String){
        var sql = ""
        if (!namaHari.trim().equals("")){
            sql = "select m.kode as _id, m.nhari, p.nama_kelas from hari m, kelas p "+
                    "where m.id_kelas=p.id_kelas and m.nhari like '%$namaHari%'"
        }else
        {
            sql = "select m.kode as _id, m.nhari, p.nama_kelas from hari m, kelas p "+
                    "where m.id_kelas=p.id_kelas order by m.nhari asc"
        }
        val c : Cursor = db.rawQuery(sql,null)
        lsAdapter = SimpleCursorAdapter(thisParent,R.layout.item_data_hari,c,
            arrayOf("_id","nhari","nama_kelas"), intArrayOf(R.id.txIdHari,R.id.txNamaHari,R.id.txNamaKelas),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsHari.adapter = lsAdapter
    }

    fun showDataKelas(){
        val c : Cursor = db.rawQuery("select nama_kelas as _id from kelas order by nama_kelas asc",null)
        spAdapter = SimpleCursorAdapter(thisParent,android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spNamaKelas.adapter = spAdapter
        v.spNamaKelas.setSelection(0)
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        spNamaKelas.setSelection(0,true)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val c = spAdapter.getItem(position) as Cursor
        namaKelas = c.getString(c.getColumnIndex("_id"))
    }

    fun insertDataHari(kode : String, nHari : String, id_kelas : Int){
        var sql = "insert into hari(kode, nHari, id_kelas) values (?,?,?)"
        db.execSQL(sql, arrayOf(kode,nHari,id_kelas))
        showDataHari("")
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select id_kelas from kelas where nama_kelas='$namaKelas'"
        var c : Cursor = db.rawQuery(sql,null)
        if (c.count>0){
            c.moveToFirst()
            insertDataHari(v.edKodeHari.text.toString(),v.edNamaHari.text.toString(),
                c.getInt(c.getColumnIndex("id_kelas")))
            v.edKodeHari.setText("")
            v.edNamaHari.setText("")
        }
    }
}
